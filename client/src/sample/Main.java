package sample;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;



public class Main extends Application{
    int counterMove = 0;
    final Text counterMoveText = new Text("0");
    final TextArea receivedMsgArea = new TextArea();
    final TextField ipText = new TextField("127.0.0.1");
    final TextField nameInput = new TextField();
    final Button nameSendButton = new Button(" Zapisz imię ");
    final Button playButton = new Button(" Zagraj ");
    final Button makeMoveButton = new Button(" Wykonaj ruch ");
    final Button resignButton = new Button(" Zrezygnuj z gry");
    final TextArea sendMsgArea = new TextArea();
    final TextField statusText = new TextField();
    final Button sendButton = new Button(" Send ");

    Player player = new Player();

    @Override
    public void start(Stage primaryStage) throws Exception {

        //board
        Pane boardRoot = new Pane();
        final int n = 8;
        final int k = 60;
        final int gap = 25;
        final int size = 2 * gap + n * k;
        Rectangle board = new Rectangle(gap, gap, n * k, n * k);
        board.setFill(Paint.valueOf("#eee"));
        boardRoot.getChildren().add(board);
        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                if (row % 2 == col % 2) {
                    int x = gap + col * k;
                    int y = gap + row * k;
                    Rectangle rect = new Rectangle(x, y, k, k);
                    rect.setFill(Color.LIGHTGREEN);
                    boardRoot.getChildren().add(rect);
                }
            }
        }

        GridPane rightPane = new GridPane();
        rightPane.setPadding(new Insets(11.5, 12.5, 13.5, 14.5));
        rightPane.setHgap(5.5);
        rightPane.setVgap(5.5);
        receivedMsgArea.setWrapText(true);
        receivedMsgArea.setEditable(false);
        receivedMsgArea.setMaxWidth(350);
        receivedMsgArea.setPrefHeight(410);
        rightPane.add(boardRoot, 0, 0);

        GridPane leftPane1 = new GridPane();
        leftPane1.setPadding(new Insets(11.5, 12.5, 13.5, 14.5));
        leftPane1.setHgap(5.5);
        leftPane1.setVgap(5.5);

        leftPane1.add(nameSendButton, 1, 0);

        leftPane1.add(nameInput, 0, 0);

        leftPane1.add(playButton, 0, 1);
        playButton.setVisible(false);

        leftPane1.add(resignButton, 0, 2);
        resignButton.setVisible(false);
        leftPane1.add(makeMoveButton, 1, 2);
        makeMoveButton.setVisible(player.canMove);

        leftPane1.add(new Label("Licznik ruchów: "), 0, 3);
        leftPane1.add(counterMoveText, 1, 3);

        GridPane leftPane2 = new GridPane();
        leftPane2.setPadding(new Insets(11.5, 12.5, 13.5, 14.5));
        leftPane2.setHgap(5.5);
        leftPane2.setVgap(5.5);
        leftPane2.add(new Label("Logi:"), 0, 2);
        sendMsgArea.setPrefHeight(250);
        sendMsgArea.setMaxWidth(275);
        sendMsgArea.setWrapText(true);
        leftPane2.add(receivedMsgArea, 0, 3);

        VBox vBox = new VBox();
        vBox.getChildren().addAll(leftPane1, leftPane2);
        HBox hBox = new HBox();
        hBox.getChildren().addAll(vBox, rightPane);

        Scene scene = new Scene(hBox);
        primaryStage.setTitle("client");
        primaryStage.setAlwaysOnTop(true);
        primaryStage.setScene(scene);
		 // Close the UI thread while closing each child thread
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                System.exit(0);
            }
        });
        primaryStage.show();

        new Thread(new Client(receivedMsgArea, makeMoveButton, resignButton, sendMsgArea, statusText, sendButton, nameInput, nameSendButton, playButton, player, counterMoveText)).start();
    }
}