package sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class Client implements Runnable {
    TextArea receivedMsgArea;
    TextArea sendMsgArea;
    TextField statusText;
    Button sendButton;
    TextField nameInput;
    Button nameSendButton;
    Button makeMoveButton;
    Button resignButton;
    Button playButton;
    Player player;
    Text counterMoveText;
    public Client() {
        super();
    }

    public Client(TextArea receivedMsgArea, Button makeMoveButton, Button resignButton, TextArea sendMsgArea,
                  TextField statusText, Button sendButton, TextField nameInput, Button nameSendButton, Button playButton, Player player, Text counterMoveText) {
        super();
        this.receivedMsgArea = receivedMsgArea;
        this.sendMsgArea = sendMsgArea;
        this.statusText = statusText;
        this.sendButton = sendButton;
        this.nameInput = nameInput;
        this.nameSendButton = nameSendButton;
        this.makeMoveButton = makeMoveButton;
        this.resignButton = resignButton;
        this.playButton = playButton;
        this.player = player;
        this.counterMoveText = counterMoveText;
    }

    public void updateCounter(Socket socket) {
        Platform.runLater(()->{

        });
    }

    public void updateLogs(String message) {
        Platform.runLater(()->{
            receivedMsgArea.appendText(message + "\n");
        });
    }

    public void updateCounter(String counter) {
        Platform.runLater(()->{
            counterMoveText.setText(counter);
        });
    }

    public void setIsPlaying(boolean isPlaying) {
        Platform.runLater(()->{
            playButton.setVisible(!isPlaying);
            resignButton.setVisible(isPlaying);
            makeMoveButton.setVisible(isPlaying);
        });
    }

    public void setCanPlay() {
        Platform.runLater(()->{
            playButton.setVisible(true);
        });
    }

    @Override
    public void run() {
        try {
            Socket socket = new Socket("127.0.0.1", 5555);

            receivedMsgArea.appendText("Połączono z serwerem" + "\n");
            InputStream in = socket.getInputStream();
            BufferedReader bReader = new BufferedReader(new InputStreamReader(in));
            OutputStream out = socket.getOutputStream();
            PrintWriter pWriter = new PrintWriter(out);

            nameSendButton.setOnAction(e->{
                String name = nameInput.getText();
                if (name != "") {
                    pWriter.write(name);
                    pWriter.flush();
                    nameSendButton.setVisible(false);
                    nameInput.setEditable(false);
                }
            });

            makeMoveButton.setOnAction(e->{
                pWriter.write("makeMove" + "\n");
                pWriter.flush();
            });

            resignButton.setOnAction(e->{
                pWriter.write("resign" + "\n");
                pWriter.flush();
            });

            playButton.setOnAction(e->{
                pWriter.write("play" + "\n");
                pWriter.flush();
            });

            String message;
            String actionType;
            String actionValue;
            boolean isAnswerBack;
            String name;
            String counter;
            List<String> messageSplited;
            List<String> action;
            while(true) {
                message = bReader.readLine();
                try {
                    messageSplited = new ArrayList<String>(Arrays.asList(message.split(" ")));
                    action = new ArrayList<String>(Arrays.asList(messageSplited.get(0).split(":")));
                    actionType = action.get(0);
                    actionValue= action.get(1);
                    name = messageSplited.get(1);
                    counter = messageSplited.get(2);
                    isAnswerBack = messageSplited.get(3).contains("answerBack");

                    if (actionType.equals("play")) {
                        if (isAnswerBack) {
                            if (actionValue.equals("can")) {
                                setIsPlaying(true);
                                updateLogs("Dołączasz do rozgrywki");
                            } else {
                                updateLogs("Wszystkie miejsca są zajęte");
                            }
                        } else {
                            if (actionValue.equals("can")) {
                                updateLogs("Użytkownik " + name + " dołączył do gry");
                            } else {
                                updateLogs("Użytkownik " + name + " chce grać ale nie ma dla niego miejsca" );
                            }
                        }
                    }
                    if (actionType.equals("resign")) {
                        if (isAnswerBack) {
                            setIsPlaying(false);
                            updateLogs("Jesteś teraz obserwatorem");
                        } else {
                            updateLogs("Użytkownik " + name + " zrezygnował z gry");
                        }
                    }
                    if (actionType.equals("init")) {
                        setCanPlay();
                        updateLogs("Pobrano dane. Możesz spróbować zając miejsce w grze.");
                    }
                    if (actionType.equals("makeMove")) {
                        if (isAnswerBack) {
                            updateLogs("Wykonałeś ruch");
                        } else {
                            updateLogs("Użytkownik " + name + " wykonał ruch");
                        }
                    }
                    updateCounter(counter);
                } catch(Exception e) {
                    updateLogs(message);
                }
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            receivedMsgArea.appendText("Serwer jest wyłączony");
            e.printStackTrace();
        }
    }

}